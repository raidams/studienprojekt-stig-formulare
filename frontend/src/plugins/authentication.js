import Keycloak from 'keycloak-js';
import { getCurrentInstance } from 'vue';

const config = {
    url: import.meta.env.VITE_KEYCLOAK_URL,
    realm: import.meta.env.VITE_KEYCLOAK_REALM,
    clientId: import.meta.env.VITE_KEYCLOAK_CLIENT_ID
};

export default {
    install: (app) => {
        const keycloak = new Keycloak(config);
        app.config.globalProperties.$keycloak = keycloak;
    },
    mounted() {
        const instance = getCurrentInstance();
        if (instance) {
            console.log(instance.appContext.config.globalProperties.$keycloak);
        }
    }
}
