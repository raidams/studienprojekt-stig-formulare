import accordion from './accordion';
import autocomplete from './autocomplete';
import avatar from './avatar';
import avatargroup from './avatargroup';
import badge from './badge';
import badgedirective from './badgedirective';
import breadcrumb from './breadcrumb';
import button from './button';
import calendar from './calendar';
import card from './card';
import cascadeselect from './cascadeselect';
import checkbox from './checkbox';
import chip from './chip';
import chips from './chips';
import confirmpopup from './confirmpopup';
import contextmenu from './contextmenu';
import datatable from './datatable';
import dataview from './dataview';
import dataviewlayoutoptions from './dataviewlayoutoptions';
import deferred from './deferred';
import dialog from './dialog';
import divider from './divider';
import dropdown from './dropdown';
import fieldset from './fieldset';
import fileupload from './fileupload';
import floatlabel from './floatlabel';
import global from './global';
import iconfield from './iconfield';
import image from './image';
import inlinemessage from './inlinemessage';
import inplace from './inplace';
import inputgroup from './inputgroup';
import inputgroupaddon from './inputgroupaddon';
import inputmask from './inputmask';
import inputnumber from './inputnumber';
import inputotp from './inputotp';
import inputswitch from './inputswitch';
import inputtext from './inputtext';
import knob from './knob';
import listbox from './listbox';
import megamenu from './megamenu';
import menu from './menu';
import menubar from './menubar';
import message from './message';
import metergroup from './metergroup';
import multiselect from './multiselect';
import orderlist from './orderlist';
import overlaypanel from './overlaypanel';
import paginator from './paginator';
import panel from './panel';
import panelmenu from './panelmenu';
import password from './password';
import progressbar from './progressbar';
import progressspinner from './progressspinner';
import radiobutton from './radiobutton';
import scrollpanel from './scrollpanel';
import scrolltop from './scrolltop';
import selectbutton from './selectbutton';
import sidebar from './sidebar';
import skeleton from './skeleton';
import speeddial from './speeddial';
import splitbutton from './splitbutton';
import splitter from './splitter';
import splitterpanel from './splitterpanel';
import stepper from './stepper';
import steps from './steps';
import tabmenu from './tabmenu';
import tabview from './tabview';
import tag from './tag';
import textarea from './textarea';
import tieredmenu from './tieredmenu';
import timeline from './timeline';
import toast from './toast';
import togglebutton from './togglebutton';
import toolbar from './toolbar';
import tooltip from './tooltip';

export default {
    global,
    directives: {
        badge: badgedirective,
        tooltip
    },

    //forms
    autocomplete,
    dropdown,
    inputnumber,
    inputtext,
    calendar,
    checkbox,
    radiobutton,
    inputswitch,
    selectbutton,
    chips,
    multiselect,
    togglebutton,
    cascadeselect,
    listbox,
    inputgroup,
    inputgroupaddon,
    inputmask,
    knob,
    textarea,
    password,
    iconfield,
    floatlabel,
    inputotp,

    //buttons
    button,
    splitbutton,
    speeddial,

    //data
    paginator,
    datatable,
    dataview,
    dataviewlayoutoptions,
    orderlist,
    timeline,

    //panels
    accordion,
    panel,
    fieldset,
    card,
    tabview,
    divider,
    toolbar,
    scrollpanel,
    splitter,
    splitterpanel,
    stepper,
    deferred,

    //file
    fileupload,

    //menu
    contextmenu,
    menu,
    menubar,
    steps,
    tieredmenu,
    breadcrumb,
    panelmenu,
    megamenu,
    tabmenu,

    //overlays
    dialog,
    overlaypanel,
    sidebar,
    confirmpopup,

    //messages
    message,
    inlinemessage,
    toast,

    //media
    image,

    //misc
    badge,
    avatar,
    avatargroup,
    tag,
    chip,
    progressbar,
    skeleton,
    scrolltop,
    metergroup,
    inplace,
    progressspinner
};
