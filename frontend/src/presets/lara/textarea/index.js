export default {
    root: ({ context, props }) => ({
        class: [
            // Font
            'font-d-din-pro text-base',

            // Spacing
            'm-0',
            'p-3',

            // Colors
            'text-primary-950 dark:text-white',
            'bg-zinc-200 dark:bg-primary-950',

            // Invalid State
            { 'border-red-500 dark:border-red-400': props.invalid },

            // States
            {
                'opacity-60 select-none pointer-events-none cursor-default': context.disabled
            },

            // Misc
            'appearance-none',
            'transition-colors duration-200'
        ]
    })
};
