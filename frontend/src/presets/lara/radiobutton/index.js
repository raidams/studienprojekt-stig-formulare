export default {
    root: {
        class: [
            'relative',

            // Flexbox & Alignment
            'inline-flex',
            'align-bottom',

            // Size
            'size-6',

            // Misc
            'cursor-pointer',
            'select-none'
        ]
    },
    box: ({ props }) => ({
        class: [
            // Flexbox
            'flex justify-center items-center',

            // Size
            'size-6',

            // Shape
            'border-2',
            'rounded-full',

            // Transition
            'transition duration-200 ease-in-out',

            // Colors
            {
                'text-zinc-200 dark:text-zinc-200': props.value !== props.modelValue && props.value !== undefined,
                'bg-zinc-200 dark:bg-zinc-200': props.value !== props.modelValue && props.value !== undefined,
                'border-surface-700 dark:border-zinc-700': props.value !== props.modelValue && props.value !== undefined && !props.invalid,
                'border-surface-700 dark:border-surface-700': props.value == props.modelValue && props.value !== undefined,
                'bg-primary-950 dark:bg-primary-950': props.value == props.modelValue && props.value !== undefined
            },
            // Invalid State
            { 'border-red-500 dark:border-red-400': props.invalid },

            // States
            {
                'peer-hover:border-surface-700 dark:peer-hover:border-surface-700': !props.disabled && !props.invalid,
                'peer-hover:border-surface-700 dark:peer-hover:border-surface-700 peer-hover:bg-primary-950 dark:peer-hover:bg-primary-950': !props.disabled && props.value == props.modelValue && props.value !== undefined,
                'peer-focus-visible:border-primary-950 dark:peer-focus-visible:border-primary-400 peer-focus-visible:ring-2 peer-focus-visible:ring-primary-400/20 dark:peer-focus-visible:ring-primary-300/20': !props.disabled,
                'opacity-100 cursor-default': props.disabled
            }
        ]
    }),
    input: {
        class: [
            'peer',

            // Size
            'w-full ',
            'h-full',

            // Position
            'absolute',
            'top-0 left-0',
            'z-10',

            // Spacing
            'p-0',
            'm-0',

            // Shape
            'opacity-0',
            'rounded-md',
            'outline-none',
            'border-2 border-zinc-200 dark:border-zinc-700',

            // Misc
            'appearance-none',
            'cursor-pointer'
        ]
    },
    icon: ({ props }) => ({
        class: [
            'block',

            // Shape
            'rounded-full',

            // Size
            'size-3',

            // Colors
            {
                'bg-primary-950 dark:bg-primary-950': props.value == props.modelValue, // Change 'your-choice-color' to the color you want when selected
                'bg-zinc-300 dark:bg-zinc-300': props.value !== props.modelValue,
            },

            // Conditions
            {
                'backface-hidden scale-10 invisible': props.value !== props.modelValue,
                'transform visible scale-[1.1]': props.value == props.modelValue
            },

            // Transition
            'transition duration-200'
        ]
    })
};
