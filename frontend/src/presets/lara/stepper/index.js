export default {
    root: ({ props }) => ({
        class: ['flex-1', props.orientation === 'vertical' ? 'flex-col' : 'flex-row']
    }),
    nav: {
        class: [
            // Flexbox
            'md:flex',
            'md:flex-row',
            'md:justify-between',
            'md:items-center',

            // Mobile
            'flex',
            'flex-col',
            'justify-left',
            'items-left',

            // Spacing
            'md:m-0',
            'md:p-0',

            // Positioning
            'md:relative',

            // Lists
            'md:list-none',

            // Overflow
            'md:overflow-x-auto'
        ]
    },
    stepperpanel: {
        panel: ({ context, parent }) => ({
            class: [context.active ? 'flex-1' : '', parent.props.orientation === 'vertical' ? 'flex flex-col flex-initial' : '']
        }),
        header: ({ parent, context }) => ({
            class: [
                // Position
                'relative',

                // Flexbox
                'flex',
                'items-center',
                context.last ? 'flex-initial' : 'flex-1',
                parent.props.orientation === 'vertical' ? 'flex-initial' : '',

                // Spacing
                'p-2'
            ]
        }),
        action: {
            class: [
                // Borders
                'border-0',
                'border-none',

                // Flexbox
                'inline-flex',
                'items-center',

                // Text
                'text-decoration-none',

                // Transitions
                'transition',
                'transition-shadow',
                'duration-200',

                // Shape
                'rounded-md',

                // Backgrounds
                'bg-transparent',

                // Focus
                'outline-none'
            ]
        },
        number: ({ context }) => ({
            class: [
                // Flexbox
                'flex',
                'items-center',
                'justify-center',

                // Colors (Conditional)
                context.active ? 'border border-primary-950 dark:border-primary-950 bg-surface-700 dark:bg-surface-700 text-white dark:text-white' : 'border border-primary-950 dark:border-primary-950 text-primary-950 dark:text-primary-950', // Adjust colors as needed

                // Size and Shape
                'size-8',
                //'line-height-[20rem]',
                //'rounded-full',

                // Text
                'font-d-din-pro',
                'text-[18px]',

                // Borders
                context.active ? 'border-solid border-2' : 'border-solid border-2',

                // Transitions
                'transition',
                'transition-colors',
                'transition-shadow',
                'duration-200'
            ]
        }),
        title: ({ context }) => ({
            class: [
                // Layout
                'block',
                'whitespace-nowrap',
                'overflow-hidden',
                'text-ellipsis',
                'max-w-full',

                // Spacing
                'ml-2',

                // Text
                context.active ? 'text-primary-950 dark:text-primary-950' : 'text-primary-950 dark:text-primary-950/80',
                'font-semibold',
                'font-d-din-pro',
                'text-[18px]',

                // Transitions
                'transition',
                'transition-colors',
                'transition-shadow',
                'duration-200'
            ]
        }),
        separator: ({ context, state, parent }) => ({
            class: [
                // Colors (Conditional for active step)
                state.d_activeStep <= context.index ? 'md:bg-zinc-100 md:dark:bg-zinc-100' : 'md:bg-primary-950 md:dark:bg-primary-950',

                // Conditional for Vertical Orientation
                parent.props.orientation === 'vertical' ? ['flex-none', 'w-[2px]', 'h-auto', 'ml-[calc(1.29rem+2px)]'] : ['flex-1', 'w-full', 'h-[2px]', 'ml-4'],

                // Transitions
                'transition-shadow',
                'duration-200'
            ]
        }),
        transition: {
            class: ['flex flex-1', 'text-primary-950 dark:text-zinc-50'],
            enterFromClass: 'max-h-0',
            enterActiveClass: 'overflow-hidden transition-[max-height] duration-1000 ease-[cubic-bezier(0.42,0,0.58,1)]',
            enterToClass: 'max-h-[1000px]',
            leaveFromClass: 'max-h-[1000px]',
            leaveActiveClass: 'overflow-hidden transition-[max-height] duration-[450ms] ease-[cubic-bezier(0,1,0,1)]',
            leaveToClass: 'max-h-0'
        },
        content: ({ parent }) => ({
            class: [parent.props.orientation === 'vertical' ? 'w-full pl-4' : '']
        })
    },
    panelcontainer: {
        class: [
            // Colors
            //'bg-surface-0 dark:bg-surface-800',
            'text-primary-950',

            // Spacing
            'p-4'
        ]
    }
};
