import { defineStore } from 'pinia';

export const useApplicationFormStore = defineStore('applicationForm', {
    state: () => ({
        personalData: {
            firstName: '',
            lastName: '',
            matriculationNumber: '',
            email: ''
        },
        degreeProgram: '',
        studyModel: null, // '2' or '3'
        spo: '',
        semester: '',
        examinationPlan: {
            createdBy: null,
            semester1: [],
            semester2: [],
            semester3: []
        },
        isExaminationPlanSelected: null
    }),
    getters: {
        isFormComplete(state) {
            return (
                state.personalData.firstName &&
                state.personalData.lastName &&
                state.personalData.matriculationNumber &&
                state.personalData.email &&
                state.degreeProgram &&
                state.studyModel &&
                state.spo &&
                state.semester &&
                state.examinationPlan
            );
        }
    },
    actions: {
        updateFirstName(name) {
            this.personalData.firstName = name;
        },
        updateLastName(name) {
            this.personalData.lastName = name;
        },
        updateMatriculationNumber(number) {
            this.personalData.matriculationNumber = number;
        },
        updateEmail(email) {
            this.personalData.email = email;
        },
        updateStudyModel(model) {
            this.studyModel = model;
        },
        updateDegreeProgram(degreeProgram) {
            this.degreeProgram = degreeProgram;
        },
        updateSPO(SPO) {
            this.spo = SPO;
        },
        updateSemester(semester) {
            this.semester = semester;
        },
        setExaminationPlan(semester, courses) {
            this.examinationPlan[`semester${semester}`] = courses;
        },
        updateCourseInPlan(semester, index, updatedCourse) {
            this.examinationPlan[`semester${semester}`][index] = updatedCourse;
        },
        getExamPlanForSemesterA() {
            const semesterModulesA = [
                ...this.examinationPlan.semester1.filter(course => course.planA).map(course => course.name),
                ...this.examinationPlan.semester2.filter(course => course.planA).map(course => course.name),
                ...this.examinationPlan.semester3.filter(course => course.planA).map(course => course.name),
            ];
            return semesterModulesA;
        },
        getExamPlanForSemesterB() {
            const semesterModulesB = [
                ...this.examinationPlan.semester1.filter(course => course.planB).map(course => course.name),
                ...this.examinationPlan.semester2.filter(course => course.planB).map(course => course.name),
                ...this.examinationPlan.semester3.filter(course => course.planB).map(course => course.name),
            ];
            return semesterModulesB;
        }
    }
});