export const emailTemplate = (message, studentEmail) => `
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h3>Student ${studentEmail} hat eine Beratung beantragt</h3>
        <span>Nachricht:</span>
        <p>${message}</p>
    </body>
</html>
`;
