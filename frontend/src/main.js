import authentication from '@/plugins/authentication';
import lara from '@/presets/lara';
import { setupTokenRefresh } from '@/services/AuthService';
import { createPinia } from 'pinia';
import PrimeVue from 'primevue/config';
import ConfirmationService from 'primevue/confirmationservice';
import ToastService from 'primevue/toastservice';
import { createApp } from 'vue';
import App from './App.vue';
import './assets/main.css';
import router from './router';

// Create the app
const app = createApp(App);

// Create the store
const pinia = createPinia();

// Set the title of the document
document.title = import.meta.env.VITE_APP_TITLE || "Vue App";

// Register plugins
app.use(authentication);
app.use(pinia);
app.use(router);
app.use(ToastService);
app.use(ConfirmationService);
app.use(PrimeVue, {
    unstyled: true,
    pt: lara
});

// Register global components
app.config.globalProperties.$keycloak
    .init({
        checkLoginIframe: false,
        onLoad: 'check-sso',
        silentCheckSsoRedirectUri: `${location.origin}/silent-check-sso.html`
    }).then(() => {
        if (app.config.globalProperties.$keycloak) {
            setupTokenRefresh(app.config.globalProperties.$keycloak);
        }
        app.mount('#app');
    }).catch(error => {
        console.error('Keycloak init failed:', error);
    });

// Export global properties
export const globals = app.config.globalProperties;

