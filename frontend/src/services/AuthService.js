import { globals } from "@/main";
let refreshInterval;

const setupTokenRefresh = (keycloak) => {
    refreshInterval = setInterval(
        () => {
            keycloak
                .updateToken(70)
                .then((refreshed) => {
                    if (refreshed) {
                        console.log("Token refreshed");
                    } else {
                        console.log(
                            "Token not refreshed, valid for " +
                            Math.round(
                                keycloak.tokenParsed.exp +
                                keycloak.timeSkew -
                                new Date().getTime() / 1000
                            ) +
                            " seconds"
                        );
                    }
                })
                .catch(() => {
                    console.error("Failed to refresh token");
                });
        },
        5 * 60 * 1000
    ); // Refresh every 5 minutes
};

const getUser = () => {
    if (!globals.$keycloak) {
        return null;
    } else {
        return globals.$keycloak.authenticated
            ? globals.$keycloak.tokenParsed
            : null;
    }
};

const cleanup = () => {
    if (refreshInterval) {
        clearInterval(refreshInterval);
    }
};

window.addEventListener("beforeunload", cleanup);

export { getUser, setupTokenRefresh };

