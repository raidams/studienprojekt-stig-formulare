import { getSemesterModules } from '@/services/ApiService';

export class StigService {
    static async getCourses(studyModel, degreeProgram, spo) {
        try {
            let courses = [];

            if (studyModel === "2") {
                const semester1ModulesResponse = await getSemesterModules(degreeProgram, spo, 1);
                const semester2ModulesResponse = await getSemesterModules(degreeProgram, spo, 2);

                const semester1Modules = semester1ModulesResponse[0].modules;
                const semester2Modules = semester2ModulesResponse[0].modules;

                courses = [
                    ...semester1Modules.map((module, index) => ({
                        id: `1-${index + 1}`,
                        name: module,
                        semester: 1,
                        passed: false,
                        grade: null,
                        attempts: 0,
                        planA: false,
                        planB: false,
                    })),
                    ...semester2Modules.map((module, index) => ({
                        id: `2-${index + 1}`,
                        name: module,
                        semester: 2,
                        passed: false,
                        grade: null,
                        attempts: 0,
                        planA: false,
                        planB: false,
                    }))
                ];
            } else if (studyModel === "3") {
                const semester1ModulesResponse = await getSemesterModules(degreeProgram, spo, 1);
                const semester2ModulesResponse = await getSemesterModules(degreeProgram, spo, 2);
                const semester3ModulesResponse = await getSemesterModules(degreeProgram, spo, 3);

                const semester1Modules = semester1ModulesResponse[0].modules;
                const semester2Modules = semester2ModulesResponse[0].modules;
                const semester3Modules = semester3ModulesResponse[0].modules;

                courses = [
                    ...semester1Modules.map((module, index) => ({
                        id: `1-${index + 1}`,
                        name: module,
                        semester: 1,
                        passed: false,
                        grade: null,
                        attempts: 0,
                        planA: false,
                        planB: false,
                    })),
                    ...semester2Modules.map((module, index) => ({
                        id: `2-${index + 1}`,
                        name: module,
                        semester: 2,
                        passed: false,
                        grade: null,
                        attempts: 0,
                        planA: false,
                        planB: false,
                    })),
                    ...semester3Modules.map((module, index) => ({
                        id: `3-${index + 1}`,
                        name: module,
                        semester: 3,
                        passed: false,
                        grade: null,
                        attempts: 0,
                        planA: false,
                        planB: false,
                    }))
                ];
            } else {
                return Promise.reject("Invalid study model");
            }

            return courses;
        } catch (error) {
            console.error('Error fetching courses:', error);
            return Promise.reject(error);
        }
    }
}

export default StigService;
