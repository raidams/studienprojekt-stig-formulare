import axios from 'axios';

const API_URL = import.meta.env.VITE_API_URL;

export const submitFormData = async (studentData) => {
    try {
        const response = await axios.post(`${API_URL}/application`, studentData, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
        console.log('Form submitted :', response.data);
        return response.data;
    } catch (error) {
        throw error;
    }
}

export const getSemesterModules = async (studentDegree, spo, semester) => {
    try {
        const response = await axios.get(`${API_URL}/examination-plan-templates`, {
            params: {
                studiengang: studentDegree,
                spo: spo,
                semester: semester
            }
        });
        return response.data;
    } catch (error) {
        throw error;
    }
}

export const submitExamPlan = async (applicationID, examPlanData) => {
    try {
        const response = await axios.post(`${API_URL}/applications/${applicationID}/examination-plans`, examPlanData, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
        console.log('Examination plan submitted:', response.data);
        return response.data;
    } catch (error) {
        console.error('Error submitting examination plan:', error.response ? error.response.data : error.message);
        throw error;
    }
}

export const getAllApplications = async () => {
    try {
        const response = await axios.get(`${API_URL}/applications`);
        return response.data;
    } catch (error) {
        throw error;
    }
}
