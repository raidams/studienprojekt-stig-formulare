import { globals } from '@/main';
import { createRouter, createWebHistory } from 'vue-router';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Startseite',
      component: () => import('../views/HomeView.vue'),
      meta: {
        isProtected: false
      }
    },
    {
      path: '/about-stig',
      name: 'Über StiG',
      component: () => import('../views/AboutView.vue'),
      meta: {
        isProtected: false
      }
    },
    {
      path: '/faqs',
      name: 'FAQs',
      component: () => import('../views/FaqView.vue'),
      meta: {
        isProtected: false
      }
    },
    {
      path: '/counsel',
      name: 'Beratungstermin',
      component: () => import('../views/CounselView.vue'),
      meta: {
        isProtected: true
      }
    },
    {
      path: '/requests',
      name: 'Aktuelle Anträge',
      component: () => import('../views/RequestsView.vue'),
      meta: {
        isProtected: true,
        requiresRole: ['studiengangkoordinator', 'studiendekan']
      }
    },
    {
      path: '/new-request',
      name: 'Neuer Antrag',
      component: () => import('../views/NewRequestView.vue'),
      meta: {
        isProtected: true,
        requiresRole: ['student', 'studiengangkoordinator', 'studiendekan']
      }
    },
    {
      path: '/profile/:id',
      name: 'profile',
      params: true,
      component: () => import('../views/ProfileView.vue'),
      meta: {
        isProtected: false
      }
    },
    {
      path: '/unauthorized',
      name: 'Unauthorized',
      component: () => import('../views/UnauthorizedView.vue'),
      meta: {
        isProtected: false
      }
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'NotFound',
      component: () => import('../views/NotFoundView.vue'),
      meta: {
        isProtected: false
      }
    },
  ]
})

router.beforeEach((to, from, next) => {
  const currentUrl = window.location;
  const basePath = currentUrl.protocol + "//" + currentUrl.host;

  if (to.meta.isProtected) {
    if (!globals.$keycloak.authenticated) {
      // Redirect to Keycloak login
      globals.$keycloak.login({ redirectUri: basePath });
    } else {
      // Check if user has at least one of the required roles
      const hasRequiredRole = to.meta.requiresRole ? to.meta.requiresRole.some(role => globals.$keycloak.hasResourceRole(role)) : true;

      if (hasRequiredRole) {
        globals.$keycloak.updateToken(70).then(() => {
          next();
        }).catch(err => {
          console.error('Token update error:', err);
          globals.$keycloak.login({ redirectUri: basePath }); // Redirect to login if token update fails
        });
      } else {
        // Redirect if not authorized
        next({ name: 'Unauthorized' });
      }
    }
  } else {
    next(); // Proceed if not protected
  }
});


export default router