/** @type {import('tailwindcss').Config} */

export default {
  content: [
    "./src/**/*.{js,jsx,ts,tsx,vue}",
  ],
  theme: {
    extend: {
      colors: {
        white: "#ffffff",
        farbe1: "#193058",
        farbe2: "#D70F3C",
        gray: "rgba(0, 0, 0, 0.15)",
        'primary': {
          '50': '#eff8ff',
          '100': '#dbeffe',
          '200': '#c0e3fd',
          '300': '#94d3fc',
          '400': '#62b9f8',
          '500': '#3d9bf4',
          '600': '#277ee9',
          '700': '#1f68d6',
          '800': '#2054ad',
          '900': '#1f4989',
          '950': '#193058',
        },
        'surface': {
          '50': '#fff1f2',
          '100': '#ffe3e4',
          '200': '#ffccd1',
          '300': '#ffa2ab',
          '400': '#ff6d7f',
          '500': '#f93a57',
          '600': '#e61841',
          '700': '#d70f3c',
          '800': '#a30e34',
          '900': '#8b1034',
          '950': '#4e0317',
        },
      },
      spacing: {},
      fontFamily: {
        "d-din-pro": "D-DIN-PRO",
      },
      borderRadius: {
        "8xs": "1px",
      },
    },
    fontSize: {
      xl: "20px",
      base: "16px",
      "5xl": "24px",
      inherit: "inherit",
    },
    screens: {
      xs: '475px',    // Extra small devices (portrait phones)
      sm: '640px',    // Small devices (landscape phones, small tablets)
      md: '768px',    // Medium devices (tablets, large phones)
      lg: '1024px',   // Large devices (laptops)
      xl: '1280px',   // Extra large devices (desktops)
    },
  },
  corePlugins: {
    preflight: false,
  },
  darkMode: "class",
}

