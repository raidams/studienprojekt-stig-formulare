package StiG.enums;

public enum Responses {
    NO_ENTRIES_FOUND {
        @Override
        public String toString() {
            return "No entries found!";
        }
    },
    APPLICATION_ALREADY_EXISTS {
        @Override
        public String toString() {
            return "Application already exists!";
        }
    },
    APPLICATION_SUBMITTED {
        @Override
        public String toString() {
            return "Application successfully created!";
        }
    },
    APPLICATION_APPROVED {
        @Override
        public String toString() {
            return "Application successfully approved!";
        }
    },
    APPLICATION_NOT_FOUND {
        @Override
        public String toString() {
            return "Application not found!";
        }
    },
    APPLICATION_SUBMISSION_FAILED {
        @Override
        public String toString() {
            return "Application submission failed. Please retry!";
        }
    },
    UPDATE_FAILED {
        @Override
        public String toString() {
            return "Update for application failed!";
        }
    },
    UPDATE_SUCCEEDED {
        @Override
        public String toString() {
            return "Application successfully updated!";
        }
    };

}
