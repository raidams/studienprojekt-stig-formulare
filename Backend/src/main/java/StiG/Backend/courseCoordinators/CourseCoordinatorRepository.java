package StiG.Backend.courseCoordinators;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface CourseCoordinatorRepository extends JpaRepository<CourseCoordinator, Integer> {

    boolean existsByEmail(String email);

    List<CourseCoordinator> findByDegreeProgramContainingIgnoreCase(String degreeProgram);

}
