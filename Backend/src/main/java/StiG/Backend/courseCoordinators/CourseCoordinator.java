package StiG.Backend.courseCoordinators;

import java.util.Date;

import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.*;

@Setter
@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString(includeFieldNames = true)
@Table(name = "course_coordinators")
public class CourseCoordinator {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "title")
    private String title;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "email", nullable = false)
    @Email(message = "Invalid email address.")
    private String email;

    @Column(name = "degreeProgram", nullable = false)
    private String degreeProgram;

    @Column(name = "faculty")
    private String faculty;

    @Column(name = "administrativeSign")
    @Pattern(regexp = "T2|T3|TB", message = "Invalid input. Please enter either 'T2', 'T3' or 'TB'.", flags = Pattern.Flag.CASE_INSENSITIVE)
    private String administrativeSign;

    @Column(name = "createdOn")
    private Date createdOn;

    @Column(name = "lastModified")
    private Date lastModified;

}
