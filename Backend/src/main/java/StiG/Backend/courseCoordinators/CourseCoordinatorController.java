package StiG.Backend.courseCoordinators;

import StiG.Backend.CommonData;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

@RestController
public class CourseCoordinatorController extends CommonData {

    @Autowired
    CourseCoordinatorRepository courseCoordinatorRepository;

    // GET /degree-program-coordinators?Studiengang={z.B. SWB}
    @GetMapping("/degree-program-coordinators")
    public ResponseEntity<Object> getDegreeProgramCoordinatorByFilter(
            @RequestParam(value = "studiengang", required = false) String degreeProgram) {
        if (courseCoordinatorRepository.findAll().isEmpty()) {
            responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            responseEntity = (degreeProgram != null)
                    ? (courseCoordinatorRepository.findByDegreeProgramContainingIgnoreCase(degreeProgram).isEmpty()
                            ? (new ResponseEntity<>("No entries found!", HttpStatus.NOT_FOUND))
                            : (new ResponseEntity<>(
                                    new ArrayList<CourseCoordinator>(
                                            courseCoordinatorRepository
                                                    .findByDegreeProgramContainingIgnoreCase(degreeProgram)),
                                    HttpStatus.FOUND)))
                    : (new ResponseEntity<>(
                            new ArrayList<CourseCoordinator>(
                                    courseCoordinatorRepository.findAll()),
                            HttpStatus.OK));
        }
        return responseEntity;
    }

    // GET /degree-program-coordinators/{coordinatorID}
    @GetMapping("/degree-program-coordinators/{coordinatorID}")
    public ResponseEntity<Object> getDegreeProgramCoordinatorByID(
            @PathVariable("coordinatorID") Integer coordinatorID) {
        if (courseCoordinatorRepository.existsById(coordinatorID)) {
            responseEntity = new ResponseEntity<>(courseCoordinatorRepository.findById(coordinatorID),
                    HttpStatus.FOUND);
        } else {
            response = "Invalid ID!";
            responseEntity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    // POST /degree-program-coordinators
    @PostMapping("/degree-program-coordinators")
    private ResponseEntity<Object> addDegreeProgramCoordinator(
            @RequestBody List<CourseCoordinator> courseCoordinators) {
        for (CourseCoordinator courseCoordinator : courseCoordinators) {
            if (courseCoordinatorRepository.existsByEmail(courseCoordinator.getEmail())) {
                response = "Course coordinator already exists!";
                responseEntity = new ResponseEntity<>(response, HttpStatus.ALREADY_REPORTED);
            } else {
                courseCoordinator.setCreatedOn(currentDate());
                courseCoordinator.setLastModified(currentDate());
                courseCoordinatorRepository.save(courseCoordinator);
                response = "Course coordinator successfully created.";
                responseEntity = new ResponseEntity<>(response, HttpStatus.CREATED);
            }
        }
        return responseEntity;
    }

    // PUT /degree-program-coordinators/{coordinatorID}
    @PutMapping("/degree-program-coordinators/{coordinatorID}")
    private ResponseEntity<Object> updateCourseCoordinator(
            @PathVariable("coordinatorID") Integer coordinatorID,
            @RequestBody CourseCoordinator coordinator) {

        if (courseCoordinatorRepository.findById(coordinatorID).isPresent()) {
            String coordinatorName = "";
            try {
                CourseCoordinator _coordinator = courseCoordinatorRepository.findById(coordinatorID).get();
                coordinatorName = _coordinator.getName() + " " + _coordinator.getSurname();
                // Check if attributes from requestbody are not null and update entity
                _coordinator.setLastModified(currentDate());
                _coordinator.setEmail(
                        Objects.nonNull(coordinator.getEmail()) ? coordinator.getEmail() : _coordinator.getEmail());
                _coordinator.setName(
                        Objects.nonNull(coordinator.getName()) ? coordinator.getName() : _coordinator.getName());
                _coordinator.setSurname(
                        Objects.nonNull(coordinator.getSurname()) ? coordinator.getSurname()
                                : _coordinator.getSurname());
                _coordinator.setDegreeProgram(
                        Objects.nonNull(coordinator.getDegreeProgram()) ? coordinator.getDegreeProgram()
                                : _coordinator.getDegreeProgram());
                // save updated entity
                courseCoordinatorRepository.save(_coordinator);
                response = "Course coordinator " + coordinatorName + " successfully updated.";
                responseEntity = new ResponseEntity<>(response, HttpStatus.ACCEPTED);
            } catch (Exception e) {
                response = "Unable to update " + coordinatorName;
                responseEntity = new ResponseEntity<>(response, HttpStatus.NOT_MODIFIED);
            }
        } else {
            response = "Course coordinator with ID " + coordinatorID + " does not exist!";
            responseEntity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        return responseEntity;
    }

    // DELETE /degree-program-coordinators/{coordinatorID}
    @DeleteMapping("/degree-program-coordinators/{coordinatorID}")
    private ResponseEntity<Object> deleteCoordinatorByID(@PathVariable("coordinatorID") Integer coordinatorID) {
        if (courseCoordinatorRepository.existsById(coordinatorID)) {
            CourseCoordinator coordinator = courseCoordinatorRepository.findById(coordinatorID).get();
            String coordinatorName = coordinator.getName() + " " + coordinator.getSurname() + ".";
            try {
                courseCoordinatorRepository.deleteById(coordinatorID);
                responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } catch (Exception e) {
                response = "Unable to delete " + coordinatorName;
                responseEntity = new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } else {
            response = "Course coordinator with ID " + coordinatorID + " not found!";
            responseEntity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }

        return responseEntity;
    }

}
