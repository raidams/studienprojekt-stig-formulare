package StiG.Backend;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import StiG.Backend.students.Student;
import StiG.Backend.students.StudentController;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@RestController
@RequestMapping("/api-stig-applications")
public class CommonData {

    StudentController studentController;

    // ############### Common methods ###############
    public Date currentDate() {
        Date dateTime = new Date();
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
        dateFormat.setTimeZone(TimeZone.getTimeZone("Europe/Berlin"));
        try {
            return dateFormat.parse(dateFormat.format(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String currentDate(String format) {
        DateFormat dateFormat = new SimpleDateFormat(format);
        String currentDate = dateFormat.format(new Date());
        return currentDate;
    }

    public String getStudentName(int applicationID) {
        String studentName = studentController.getStudentNameByApplicationID(applicationID);
        return studentName;
    };

    public String updateCreatedBy(Student student) {
        String username = (student.getApplication().getCreatedBy() == null)
                ? student.getStudentNumber().toString()
                : student.getApplication().getCreatedBy();
        return username;
    }

    public String updateLastModifiedBy(Student student) {
        String username = (student.getApplication().getLastModifiedBy() == null)
                ? student.getStudentNumber().toString()
                : student.getApplication().getLastModifiedBy();
        return username;
    }

    // ###############

    // ############### Common variables ###############
    protected String response;

    protected ResponseEntity<Object> responseEntity = null;
    // ###############

}
