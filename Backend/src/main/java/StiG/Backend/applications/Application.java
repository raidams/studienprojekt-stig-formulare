package StiG.Backend.applications;

import java.util.Date;

import StiG.Backend.examinationPlan.ExamPlan;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.*;
import jakarta.persistence.*;
import lombok.*;

@Setter
@Getter
@Entity
@Data
@Transactional
@NoArgsConstructor
@AllArgsConstructor
@ToString(includeFieldNames = true)
@Table(name = "applications")
public class Application {

    // ############### Relations ###############

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "examPlanID", referencedColumnName = "examinationPlanID")
    private ExamPlan examPlan;

    // ###############

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "applicationID")
    private Integer applicationID;

    @Column(name = "stigSemester")
    @Min(value = 2, message = "Invalid input. Please enter either '2' or '3'.")
    @Max(value = 3, message = "Invalid input. Please enter either '2' or '3'.")
    private int stigSemester;

    @Column(name = "semesterA")
    @Pattern(regexp = "Sommersemester|Wintersemester", message = "Invalid input. Please enter either 'Sommersemester' or 'Wintersemester'.", flags = Pattern.Flag.CASE_INSENSITIVE)
    private String semesterA;

    @Column(name = "semesterB")
    @Pattern(regexp = "Sommersemester|Wintersemester", message = "Invalid input. Please enter either 'Sommersemester' or 'Wintersemester'.", flags = Pattern.Flag.CASE_INSENSITIVE)
    private String semesterB;

    @Column(name = "approvedByStudent")
    private boolean approvedByStudent;

    @Column(name = "approvedByDekan")
    private boolean approvedByDekan;

    @Column(name = "ApprovedByDekanOn")
    private Date approvedByDekanOn;

    @Column(name = "createdOn")
    private Date createdOn;

    @Column(name = "createdBy")
    private String createdBy;

    @Column(name = "lastModified")
    private Date lastModified;

    @Column(name = "lastModifiedBy")
    private String lastModifiedBy;

}
