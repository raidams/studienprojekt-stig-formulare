package StiG.Backend.applications;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import StiG.Backend.CommonData;
import StiG.Backend.serviceContracts.ApplicationContract;
import StiG.Backend.students.Student;
import StiG.Backend.students.StudentController;
import StiG.enums.Responses;

@RestController
public class ApplicationController extends CommonData implements ApplicationContract {

    private static final Logger logger = LoggerFactory.getLogger(ApplicationController.class);

    @Autowired
    private ApplicationRepository applicationRepository;

    @Autowired
    private StudentController studentController;

    // nach Datum filtern, StigSemester
    // GET /applications?Studiengang={mein Studiengang, z.B. SWB}
    @GetMapping("/applications")
    @Override
    public ResponseEntity<Object> getAllApplications(
            @RequestParam(value = "degreeProgram", required = false) Optional<String> degreeProgram,
            @RequestParam(value = "stigSemester", required = false) Optional<Integer> stigSemester,
            @RequestParam(value = "applicationID", required = false) Optional<Integer> applicationID,
            @RequestParam(value = "studentNumber", required = false) Optional<Integer> studentNumber) {

        if (!degreeProgram.isPresent() && !stigSemester.isPresent() &&
                !applicationID.isPresent() && !studentNumber.isPresent()) {
            responseEntity = studentController.getAllApplications();
        } else {
            responseEntity = studentController.getApplicationsByFilter(degreeProgram, stigSemester, applicationID,
                    studentNumber);
        }
        return responseEntity;
    }

    // POST /applications
    @PostMapping("/application")
    @Override
    public ResponseEntity<Object> createApplication(@RequestBody List<Student> students) {
        if (students.isEmpty()) {
            response = "Request body is empty!";
            responseEntity = new ResponseEntity<>(response, HttpStatus.NOT_ACCEPTABLE);
        } else {
            for (Student _student : students) {
                Application application = _student.getApplication();
                application.setCreatedBy(updateCreatedBy(_student));
                application.setCreatedOn(currentDate());
                application.setLastModified(currentDate());
                application.setLastModifiedBy(updateLastModifiedBy(_student));
                if (application.isApprovedByDekan()) {
                    application.setApprovedByDekanOn(currentDate());
                }
                if (application.getExamPlan() != null) {
                    application.getExamPlan().setCreatedOn(currentDate());
                    application.getExamPlan().setLastModified(currentDate());
                }
                _student.setApplication(application);
                responseEntity = studentController.addStudent(_student);
            }
        }
        return responseEntity;
    }

    // POST /applications/{id}/approvals
    @PostMapping("/applications/{applicationID}/approvals")
    @Override
    public ResponseEntity<Object> approveApplication(
            @PathVariable Integer applicationID,
            @RequestBody Application application) {

        if (applicationRepository.existsById(applicationID)) {
            Application _application = applicationRepository.findById(applicationID).get();
            _application.setApprovedByDekanOn(currentDate());
            if (application.isApprovedByStudent()) {
                _application.setApprovedByStudent(application.isApprovedByStudent());
            }
            if (application.isApprovedByDekan()) {
                _application.setApprovedByDekan(application.isApprovedByDekan());
            }
            applicationRepository.save(_application);
            logger.info("Application {} approved by {}", application.getApplicationID(), application.getCreatedBy());
            responseEntity = new ResponseEntity<>(Responses.APPLICATION_APPROVED, HttpStatus.ACCEPTED);
        } else {
            responseEntity = new ResponseEntity<>(Responses.APPLICATION_NOT_FOUND, HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    // PUT /applications/{id}
    @PutMapping("/applications/{applicationID}")
    @Override
    public ResponseEntity<Object> updateApplication(@PathVariable("applicationID") Integer applicationID,
            @RequestBody Application application) {

        if (applicationRepository.findById(applicationID).isPresent()) {
            Application _application = applicationRepository.findById(applicationID).get();
            _application.setStigSemester(
                    Objects.nonNull(application.getStigSemester()) ? application.getStigSemester()
                            : _application.getStigSemester());
            _application.setSemesterA(
                    Objects.nonNull(application.getSemesterA()) ? application.getSemesterA()
                            : _application.getSemesterA());
            _application.setSemesterB(
                    Objects.nonNull(application.getSemesterB()) ? application.getSemesterB()
                            : _application.getSemesterB());
            _application.setLastModified(currentDate());
            try {
                applicationRepository.save(_application);
                logger.info("Application {} updated: {}", applicationID, _application);
                responseEntity = new ResponseEntity<>(Responses.UPDATE_SUCCEEDED, HttpStatus.ACCEPTED);
            } catch (Exception e) {
                logger.error("Unable to update application {} \nError message -> {}", applicationID, e.getMessage());
                responseEntity = new ResponseEntity<>(Responses.UPDATE_FAILED, HttpStatus.NOT_MODIFIED);
            }
        } else {
            logger.warn("Unable to update application with id {}: {} -> Application does not exist.", applicationID,
                    application);
            responseEntity = new ResponseEntity<>(Responses.APPLICATION_NOT_FOUND, HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

    public boolean applicationRepositoryIsEmpty() {
        return applicationRepository.findAll().isEmpty();
    }

}
