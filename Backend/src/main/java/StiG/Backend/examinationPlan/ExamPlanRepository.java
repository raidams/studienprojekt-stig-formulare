package StiG.Backend.examinationPlan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ExamPlanRepository extends JpaRepository<ExamPlan, Integer>{
    
}
