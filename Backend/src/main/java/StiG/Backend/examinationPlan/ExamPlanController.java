package StiG.Backend.examinationPlan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import StiG.Backend.CommonData;
import StiG.Backend.applications.Application;
import StiG.Backend.applications.ApplicationRepository;
import StiG.Backend.serviceContracts.ExaminationPlanContract;

@RestController
public class ExamPlanController extends CommonData implements ExaminationPlanContract {

  @Autowired
  private ExamPlanRepository examPlanRepository;

  @Autowired
  private ApplicationRepository applicationRepository;

  @GetMapping("/applications/{applicationID}/examination-plans/{examPlanID}")
  @Override
  public ResponseEntity<Object> getExamPlans(
      @PathVariable("applicationID") int applicationID,
      @PathVariable("examPlanID") int examPlanID) {

    if (examPlanRepository.findAll().isEmpty()) {
      responseEntity = new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } else {
      if (applicationRepository.existsById(applicationID)) {
        if (examPlanRepository.existsById(examPlanID)) {
          responseEntity = new ResponseEntity<>(examPlanRepository.findById(examPlanID).get(), HttpStatus.ACCEPTED);
        } else {
          response = "Examination plan for " + getStudentName(applicationID) + " does not exist!";
          responseEntity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
        }
      } else {
        response = "Application with ID " + applicationID + " not found!";
        responseEntity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
      }
    }
    return responseEntity;
  }

  @PostMapping("/applications/{applicationID}/examination-plans")
  @Override
  public ResponseEntity<Object> addExaminationPlan(
      @PathVariable("applicationID") int applicationID,
      @RequestBody ExamPlan examPlan) {

    if (applicationRepository.existsById(applicationID)) {
      try {
        Application application = applicationRepository.findById(applicationID).get();
        if (application.getExamPlan() == null) {
          examPlan.setCreatedOn(currentDate());
          examPlan.setLastModified(currentDate());
          application.setExamPlan(examPlan);
          applicationRepository.save(application);
          response = "Examination plan for " + getStudentName(applicationID) + " successfully created.";
          responseEntity = new ResponseEntity<>(response, HttpStatus.CREATED);
        } else {
          response = "Examination plan already exists.";
          responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
        }
      } catch (Exception e) {
        response = "Unable to save examination plan for " + getStudentName(applicationID) + "!";
        responseEntity = new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
      }
    } else {
      response = "Application with ID " + applicationID + " not found!";
      responseEntity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }
    return responseEntity;
  }

  @PutMapping("/applications/{applicationID}/examination-plans/{examinationPlanID}")
  @Override
  public ResponseEntity<Object> updateExaminationPlan(
      @PathVariable("applicationID") int applicationID,
      @PathVariable("examinationPlanID") int examinationPlanID,
      @RequestBody ExamPlan examPlan) {

    if (applicationRepository.existsById(applicationID)) {
      if (examPlanRepository.existsById(examinationPlanID)) {
        try {
          ExamPlan _examPlan = examPlanRepository.findById(examinationPlanID).get();
          _examPlan.setLastModified(currentDate());
          _examPlan.setSemesterModulesA(examPlan.getSemesterModulesA());
          _examPlan.setSemesterModulesB(examPlan.getSemesterModulesB());
          examPlanRepository.save(_examPlan);
          response = "Examination Plan for " + getStudentName(applicationID) + " successfully updated!";
          responseEntity = new ResponseEntity<>(response, HttpStatus.OK);
        } catch (Exception e) {
          response = "Unable to save examination Plan for " + getStudentName(applicationID) + "!";
          responseEntity = new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
      } else {
        response = "Examination Plan for " + getStudentName(applicationID) + " does not exist!";
        responseEntity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
      }
    } else {
      response = "Application with ID " + applicationID + " not found!";
      responseEntity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }
    return responseEntity;
  }

}
