package StiG.Backend.examinationPlan;

import java.util.Date;

import jakarta.persistence.*;
import lombok.*;

@Setter
@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString(includeFieldNames = true)
@Table(name = "examination_plan")
public class ExamPlan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "examinationPlanID")
    private int examinationPlanID;

    @Column(name = "semesterModulesA")
    private String semesterModulesA[];

    @Column(name = "semesterModulesB")
    private String semesterModulesB[];

    @Column(name = "createdOn")
    private Date createdOn;

    @Column(name = "createdBy")
    private String createdBy;

    @Column(name = "lastModified")
    private Date lastModified;

    @Column(name = "lastModifiedBy")
    private String lastModifiedBy;

}
