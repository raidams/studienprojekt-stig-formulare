package StiG.Backend.students;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;
import StiG.Backend.examinationPlanTemplates.ModuleRepository;
import StiG.Backend.serviceContracts.StudentContract;
import StiG.enums.Responses;

import org.springframework.http.*;
import StiG.Backend.CommonData;
import StiG.Backend.examinationPlanTemplates.Module;

@RestController
public class StudentController extends CommonData implements StudentContract {
    private static final Logger logger = LoggerFactory.getLogger(StudentController.class);

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private ModuleRepository moduleRepository;

    @Override
    public ResponseEntity<Object> getAllApplications() {
        try {
            responseEntity = studentRepository.findAll().isEmpty()
                    ? new ResponseEntity<>("No applications submitted!", HttpStatus.NOT_FOUND)
                    : new ResponseEntity<>(studentRepository.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            responseEntity = new ResponseEntity<>(studentRepository.findAll(), HttpStatus.OK);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Object> getApplicationsByFilter(
            Optional<String> degreeProgram,
            Optional<Integer> stigSemester,
            Optional<Integer> applicationID,
            Optional<Integer> studentNumber) {

        Specification<Student> spec = Specification.where(
                degreeProgram.map(StudentSpecification::hasDegreeProgram).orElse(null))
                .and(stigSemester.map(StudentSpecification::hasStigSemester).orElse(null))
                .and(applicationID.map(StudentSpecification::hasApplicationID).orElse(null))
                .and(studentNumber.map(StudentSpecification::hasStudentNumber).orElse(null));

        List<Student> students = studentRepository.findAll(spec);
        if (students.isEmpty()) {
            responseEntity = new ResponseEntity<>(Responses.NO_ENTRIES_FOUND, HttpStatus.NO_CONTENT);
        } else {
            responseEntity = new ResponseEntity<>(students, HttpStatus.OK);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Object> addStudent(Student student) {
        String studentName = student.getName() + " " + student.getSurname();
        if (studentRepository.existsById(student.getStudentNumber())) {
            responseEntity = new ResponseEntity<>(Responses.APPLICATION_ALREADY_EXISTS,
                    HttpStatus.CONFLICT);
        } else {
            try {
                Module module = moduleRepository
                        .findByDegreeProgramContainingIgnoreCaseAndSpo(student.getDegreeProgram(), student.getSpo())
                        .stream()
                        .findFirst()
                        .orElse(null);
                student.setDegreeProgram(module.getDegreeProgram());
                try {
                    studentRepository.save(student);
                    logger.info("New application form submitted: '{}'", student);
                    responseEntity = new ResponseEntity<>(Responses.APPLICATION_SUBMITTED, HttpStatus.CREATED);
                } catch (Exception e) {
                    logger.error("Application submission failed for {}: {}", student);
                    responseEntity = new ResponseEntity<>(Responses.APPLICATION_SUBMISSION_FAILED,
                            HttpStatus.INTERNAL_SERVER_ERROR);
                }
            } catch (Exception e) {
                response = "Application for " + studentName + " failed! \nDegree program or spo does not exist";
                responseEntity = new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
            }
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Object> getApplicationByDegreeProgram(String degreeProgram) {
        try {
            responseEntity = studentRepository.findByDegreeProgramContainingIgnoreCase(degreeProgram).isEmpty()
                    ? new ResponseEntity<>(Responses.NO_ENTRIES_FOUND, HttpStatus.NOT_FOUND)
                    : new ResponseEntity<>(studentRepository.findByDegreeProgramContainingIgnoreCase(degreeProgram),
                            HttpStatus.FOUND);
        } catch (Exception e) {
            responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Object> getApplicationByStigSemester(Integer stigSemester) {
        try {
            responseEntity = studentRepository.findByApplicationStigSemester(stigSemester).isEmpty()
                    ? new ResponseEntity<>(Responses.NO_ENTRIES_FOUND, HttpStatus.NOT_FOUND)
                    : new ResponseEntity<>(studentRepository.findByApplicationStigSemester(stigSemester),
                            HttpStatus.FOUND);
        } catch (Exception e) {
            responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Object> getApplicationByDegreeProgramAndStigSemester(String degreeProgram,
            Integer stigSemester) {
        try {
            responseEntity = studentRepository.findByDegreeProgramContainingIgnoreCase(degreeProgram).isEmpty()
                    ? new ResponseEntity<>(Responses.NO_ENTRIES_FOUND, HttpStatus.NOT_FOUND)
                    : new ResponseEntity<>(
                            studentRepository.findByDegreeProgramContainingIgnoreCaseAndApplicationStigSemester(
                                    degreeProgram, stigSemester),
                            HttpStatus.FOUND);
        } catch (Exception e) {
            responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<Object> getApplicationByCreatedOn(Date createdOn) {
        try {
            responseEntity = studentRepository.findByApplicationCreatedOn(createdOn).isEmpty()
                    ? new ResponseEntity<>(Responses.NO_ENTRIES_FOUND, HttpStatus.NOT_FOUND)
                    : new ResponseEntity<>(studentRepository.findByApplicationCreatedOn(createdOn),
                            HttpStatus.FOUND);
        } catch (Exception e) {
            responseEntity = new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return responseEntity;
    }

    @Override
    public Student getStudentByApplicationID(Integer applicationID) {
        return studentRepository.findByApplicationApplicationID(applicationID);
    }

    @Override
    public String getStudentNameByApplicationID(Integer applicationID) {
        Student student = studentRepository.findByApplicationApplicationID(applicationID);
        return student.getName() + " " + student.getSurname();
    }

    @Override
    public ResponseEntity<Object> getApplicationByStudentNumber(Integer studentNumber) {
        if (studentRepository.existsById(studentNumber)) {
            responseEntity = new ResponseEntity<>(studentRepository.findByStudentNumber(studentNumber),
                    HttpStatus.FOUND);
        } else {
            responseEntity = new ResponseEntity<>(Responses.APPLICATION_NOT_FOUND, HttpStatus.NOT_FOUND);
        }
        return responseEntity;
    }

}
