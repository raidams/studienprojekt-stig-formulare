package StiG.Backend.students;

import org.springframework.data.jpa.domain.Specification;

public class StudentSpecification {

    public static Specification<Student> hasDegreeProgram(String degreeProgram) {
        return (root, query, criteriaBuilder) -> degreeProgram == null ? null
                : criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("degreeProgram")), "%" + degreeProgram.toLowerCase() + "%");
    }

    public static Specification<Student> hasStigSemester(Integer stigSemester) {
        return (root, query, criteriaBuilder) -> stigSemester == null ? null
                : criteriaBuilder.equal(root.get("application").get("stigSemester"), stigSemester);
    }

    public static Specification<Student> hasApplicationID(Integer applicationID) {
        return (root, query, criteriaBuilder) -> applicationID == null ? null
                : criteriaBuilder.equal(root.get("application").get("applicationID"), applicationID);
    }

    public static Specification<Student> hasStudentNumber(Integer studentNumber) {
        return (root, query, criteriaBuilder) -> studentNumber == null ? null
                : criteriaBuilder.equal(root.get("studentNumber"), studentNumber);
    }
}
