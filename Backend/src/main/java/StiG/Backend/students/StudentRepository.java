package StiG.Backend.students;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer>, JpaSpecificationExecutor<Student> {

    Student findByStudentNumber(Integer studentNumber);

    List<Student> findByDegreeProgramContainingIgnoreCase(String degreeProgram);

    Student findByApplicationApplicationID(Integer applicationID);

    List<Student> findByApplicationStigSemester(Integer stigSemester);

    List<Student> findByDegreeProgramContainingIgnoreCaseAndApplicationStigSemester(String degreeProgram,
            Integer stigSemester);

    List<Student> findByApplicationCreatedOn(Date createdOn);

}
