package StiG.Backend.students;

import StiG.Backend.applications.Application;
import jakarta.persistence.*;
import jakarta.transaction.Transactional;
import jakarta.validation.constraints.*;
import lombok.*;

@Setter
@Getter
@Entity
@Data
@Transactional
@NoArgsConstructor
@AllArgsConstructor
@ToString(includeFieldNames = true)
@Table(name = "students")
public class Student {

    // ############### Relations ###############

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "application_id", referencedColumnName = "applicationID")
    private Application application;

    // ###############

    @Id
    @Column(name = "studentNumber", unique = true)
    @NotNull(message = "Student number is invalid !!")
    @Digits(integer = 6, fraction = 0, message = "Student number must have up to {integer} digits")
    @Min(value = 100000, message = "Student number incomplete. Must have exactly 6 digits.")
    @Max(value = 999999, message = "Student number incomplete. Must have exactly 6 digits.")
    private Integer studentNumber;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "email")
    @Email(message = "Invalid email address.")
    private String email;

    @Column(name = "degreeProgram", nullable = false)
    private String degreeProgram;

    @Column(name = "spo", nullable = false)
    private Integer spo;

}
