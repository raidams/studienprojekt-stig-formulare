package StiG.Backend.serviceContracts;

import org.springframework.http.ResponseEntity;

import StiG.Backend.examinationPlan.ExamPlan;

public interface ExaminationPlanContract {

    ResponseEntity<Object> getExamPlans(int applicationID, int examPlanID);

    ResponseEntity<Object> addExaminationPlan(int applicationID, ExamPlan examPlan);

    ResponseEntity<Object> updateExaminationPlan(int applicationID, int examinationPlanID, ExamPlan examPlan);

}
