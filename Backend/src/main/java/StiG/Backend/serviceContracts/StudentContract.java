package StiG.Backend.serviceContracts;

import java.util.Date;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import StiG.Backend.students.Student;

public interface StudentContract {

    ResponseEntity<Object> getAllApplications();

    ResponseEntity<Object> getApplicationsByFilter(
            Optional<String> degreeProgram,
            Optional<Integer> stigSemester,
            Optional<Integer> applicationID,
            Optional<Integer> studentNumber);

    ResponseEntity<Object> addStudent(Student student);

    ResponseEntity<Object> getApplicationByStudentNumber(Integer studentNumber);

    ResponseEntity<Object> getApplicationByDegreeProgram(String degreeProgram);

    ResponseEntity<Object> getApplicationByStigSemester(Integer stigSemester);

    ResponseEntity<Object> getApplicationByDegreeProgramAndStigSemester(String degreeProgram, Integer stigSemester);

    ResponseEntity<Object> getApplicationByCreatedOn(Date createdOn);

    String getStudentNameByApplicationID(Integer applicationID);

    Student getStudentByApplicationID(Integer applicationID);

}
