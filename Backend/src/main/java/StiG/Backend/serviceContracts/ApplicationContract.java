package StiG.Backend.serviceContracts;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;

import StiG.Backend.applications.Application;
import StiG.Backend.students.Student;

public interface ApplicationContract {

    ResponseEntity<Object> getAllApplications(
            Optional<String> degreeProgram,
            Optional<Integer> stigSemester,
            Optional<Integer> applicationID,
            Optional<Integer> studentNumber);

    ResponseEntity<Object> createApplication(List<Student> students);

    ResponseEntity<Object> approveApplication(Integer applicationID, Application application);

    ResponseEntity<Object> updateApplication(Integer applicationID, Application application);

}
