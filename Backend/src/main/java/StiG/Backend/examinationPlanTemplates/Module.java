package StiG.Backend.examinationPlanTemplates;

import jakarta.persistence.*;
import lombok.*;

@Setter
@Getter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@ToString(includeFieldNames = true)
@Table(name="semester_modules")
public class Module {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name="semester")
    private int semester;

    @Column(name="degreeProgram")
    private String degreeProgram;

    @Column(name="modules")
    private String modules [];

    @Column(name="spo")
    private int spo; 


}
