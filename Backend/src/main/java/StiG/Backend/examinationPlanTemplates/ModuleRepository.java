package StiG.Backend.examinationPlanTemplates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ModuleRepository extends JpaRepository<Module, Integer> {

    List<Module> findByDegreeProgramContainingIgnoreCaseAndSemesterAndSpo(String degreeProgram, Integer semester,
            Integer spo);

    List<Module> findByDegreeProgramContainingIgnoreCaseAndSemester(String degreeProgram, Integer semester);

    List<Module> findByDegreeProgramContainingIgnoreCaseAndSpo(String degreeProgram, Integer spo);

    List<Module> findBySpoAndSemester(Integer spo, Integer semester);

    List<Module> findByDegreeProgramContainingIgnoreCase(String degreeProgram);

    List<Module> findBySemester(Integer semester);

    List<Module> findBySpo(Integer spo);

}
