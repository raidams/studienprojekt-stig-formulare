package StiG.Backend.examinationPlanTemplates;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import StiG.Backend.CommonData;

@RestController
public class ModuleController extends CommonData {

    @Autowired
    private ModuleRepository moduleRepository;

    // ?Studiengang={degree_course}&SPO={spo}&Semester={semester}

    @GetMapping("/examination-plan-templates")
    public List<Module> getAllModules(
            @RequestParam(value = "studiengang", required = false) String degreeProgram,
            @RequestParam(value = "semester", required = false) Integer semester,
            @RequestParam(value = "spo", required = false) Integer spo) {

        if (degreeProgram != null && semester != null && spo != null) {
            return moduleRepository.findByDegreeProgramContainingIgnoreCaseAndSemesterAndSpo(degreeProgram, semester,
                    spo);
        } else if (degreeProgram != null && semester != null) {
            return moduleRepository.findByDegreeProgramContainingIgnoreCaseAndSemester(degreeProgram, semester);
        } else if (degreeProgram != null && spo != null) {
            return moduleRepository.findByDegreeProgramContainingIgnoreCaseAndSpo(degreeProgram, spo);
        } else if (spo != null && semester != null) {
            return moduleRepository.findBySpoAndSemester(spo, semester);
        } else if (degreeProgram != null) {
            return moduleRepository.findByDegreeProgramContainingIgnoreCase(degreeProgram);
        } else if (semester != null) {
            return moduleRepository.findBySemester(semester);
        } else if (spo != null) {
            return moduleRepository.findBySpo(spo);
        } else {
            return moduleRepository.findAll();
        }
    }

    @PostMapping("/examination-plan-templates")
    public String addModule(@RequestBody List<Module> module) {
        moduleRepository.saveAll(module);
        return "Module added successfully!";
    }

    @PutMapping("/examination-plan-templates/{id}")
    public String updateModule(@PathVariable("id") Integer id, @RequestBody Module module) {

        if (moduleRepository.findById(id).isPresent()) {
            Module _module = moduleRepository.findById(id).get();
            _module.setSemester(module.getSemester());
            _module.setDegreeProgram(module.getDegreeProgram());
            _module.setModules(module.getModules());
            _module.setSpo(module.getSpo());
            moduleRepository.save(_module);
            response = "Module successfully updated.";
        } else {
            response = "Invalid module id!";
        }
        return response;
    }

}
