--
-- PostgreSQL database dump
--

-- Dumped from database version 16.2 (Debian 16.2-1.pgdg120+2)
-- Dumped by pg_dump version 16.2 (Debian 16.2-1.pgdg120+2)

CREATE DATABASE stig_auth;

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: semester_modules; Type: TABLE; Schema: public; Owner: hs_esslingen
--

CREATE TABLE public.semester_modules (
    id integer NOT NULL,
    degree_program character varying(255),
    modules character varying(255)[],
    semester integer,
    spo integer
);


ALTER TABLE public.semester_modules OWNER TO hs_esslingen;

--
-- Name: semester_modules_id_seq; Type: SEQUENCE; Schema: public; Owner: hs_esslingen
--

CREATE SEQUENCE public.semester_modules_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE public.semester_modules_id_seq OWNER TO hs_esslingen;

--
-- Name: semester_modules_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: hs_esslingen
--

ALTER SEQUENCE public.semester_modules_id_seq OWNED BY public.semester_modules.id;


--
-- Name: semester_modules id; Type: DEFAULT; Schema: public; Owner: hs_esslingen
--

ALTER TABLE ONLY public.semester_modules ALTER COLUMN id SET DEFAULT nextval('public.semester_modules_id_seq'::regclass);


--
-- Data for Name: semester_modules; Type: TABLE DATA; Schema: public; Owner: hs_esslingen
--

COPY public.semester_modules (id, degree_program, modules, semester, spo) FROM stdin;
1	Technische Informatik (TIB)	{"Mathematik 1A","Mathematik 1B","Elektotechnik 1",Betriebswirtschaftslehre,Programmieren}	1	7
2	Technische Informatik (TIB)	{"Mathematik 2","Digitaltechnik 1","Elektotechnik 2",Betriebssysteme,"Objektorientierte Systeme 1",Statistik}	2	7
3	Technische Informatik (TIB)	{"Digitaltechnik 2",Rechnernetze,"Signale und Systeme",Softwaretechnik,Elektronik,Physik}	3	7
4	Softwaretechnik und Medieninformatik (SWB)	{"Mathematik 1A","Mathematik 1B",Betriebswirtschaftslehre,Informationstechnik,Programmieren}	1	7
5	Softwaretechnik und Medieninformatik (SWB)	{"Mathematik 2","Mensch-Computer-Interaktion 1","Diskrete Mathematik",Betriebssysteme,"Objektorientierte Systeme 1",Statistik}	2	7
6	Softwaretechnik und Medieninformatik (SWB)	{"Objektorientierte Systeme 2",Softwaretechnik,"Modellbildung und Simulation",Rechnernetze,"Datenbanken 1","Internet Technologien"}	3	7
7	Wirtschaftsinformatik (WKB)	{"Mathematik 1A","Mathematik 1B","Wirtschaftsinformatik 1","BWL und VWL",Programmieren}	1	5
8	Wirtschaftsinformatik (WKB)	{"Mathematik 2",Statistik,"Wirtschaftsinformatik 2","Rechnungswesen 1",Informationstechnik,"Objektorientierte Systeme"}	2	5
9	Wirtschaftsinformatik (WKB)	{"Geschäftsprozesse 1",Rechnernetze,"Datenbanken 1",Softwaretechnik,"Rechnungswesen 2","Mensch-Computer-Interaktion 1"}	3	5
10	IT-Sicherheit (ISB)	{"Mathematik 1A","Mathematik 1B","IT Security",Informationstechnik,Programmieren}	1	1
11	IT-Sicherheit (ISB)	{"Mathematik 2",Statistik,"Offensive Sicherheit",Betriebssysteme,"Diskrete Mathematik","Objektorientierte Systeme 1"}	2	1
12	IT-Sicherheit (ISB)	{"Safety and Security",Kryptografie,"Datenbanken 1",Rechnernetze,Softwaretechnik,"Internet Technology"}	3	1
13	Ingenieurpädagogik Informationstechnik-Elektrotechnik (IEP)	{"Elektrotechnik 1","Mathematik 1A","Mathematik 1B",Programmieren}	1	7
14	Ingenieurpädagogik Informationstechnik-Elektrotechnik (IEP)	{"Digitaltechnik 1","Elektrotechnik 2",Betriebssysteme,"Mathematik 2",Statistik,"Objektorientierte Systeme 1"}	2	7
15	Ingenieurpädagogik Informationstechnik-Elektrotechnik (IEP)	{"Signale und Systeme","Digitaltechnik 2",Elektronik,Physik,Rechnernetze,Softwaretechnik}	3	7
\.


--
-- Name: semester_modules_id_seq; Type: SEQUENCE SET; Schema: public; Owner: hs_esslingen
--

SELECT pg_catalog.setval('public.semester_modules_id_seq', 15, true);


--
-- Name: semester_modules semester_modules_pkey; Type: CONSTRAINT; Schema: public; Owner: hs_esslingen
--

ALTER TABLE ONLY public.semester_modules
    ADD CONSTRAINT semester_modules_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

