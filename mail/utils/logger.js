const bunyan = require("bunyan");
export const logger = bunyan.createLogger({
    name: "mail-service",
    streams: [
        {
            level: "info",
            path: "/usr/src/app/logs/infos.log",
        },
        {
            level: "error",
            path: "/usr/src/app/logs/errors.log",
        },
    ],
});