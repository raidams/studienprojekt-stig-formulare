import cors from 'cors';
import dotenv from 'dotenv';
import express from 'express';
import nodemailer from 'nodemailer';
import { logger } from './utils/logger.js';

dotenv.config();
const app = express();
const server_port = process.env.NODEMAILER_SERVER_PORT;

app.use(cors());
app.use(express.json());

var transporter = nodemailer.createTransport({
    host: process.env.NODEMAILER_HOST,
    port: process.env.NODEMAILER_PORT,
    auth: {
        user: process.env.NODEMAILER_USER,
        pass: process.env.NODEMAILER_PASSWORD
    }
});

app.post('/send-email', async (req, res) => {
    const { senderName, senderAddress, to, subject, html } = req.body;
    try {
        await transporter.sendMail({
            from: {
                name: senderName,
                address: senderAddress
            },
            to,
            subject,
            html
        });
        logger.info(`Email sent from ${senderAddress} to ${to} with subject ${subject}`);
        res.status(200).send('Email sent successfully');
    } catch (error) {
        logger.error(`Error sending email: ${error}`);
        res.status(500).send('Error sending email');
    }
});

app.get('/', (req, res) => {
    res.send('Mail service is running');
});

app.listen(server_port, () => {
    logger.info(`Mail service is running on port ${server_port}`);
});
