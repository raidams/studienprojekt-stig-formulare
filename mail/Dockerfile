# Use the official Bun image
# See all versions at https://hub.docker.com/r/oven/bun/tags
FROM oven/bun:1-alpine as base

# Define build arguments for environment variables
ARG NODEMAILER_HOST
ARG NODEMAILER_PORT
ARG NODEMAILER_USER
ARG NODEMAILER_PASSWORD
ARG NODEMAILER_SERVER_PORT

# Pass environment variables to the container
ENV NODEMAILER_HOST=$NODEMAILER_HOST
ENV NODEMAILER_PORT=$NODEMAILER_PORT
ENV NODEMAILER_USER=$NODEMAILER_USER
ENV NODEMAILER_PASSWORD=$NODEMAILER_PASSWORD
ENV NODEMAILER_SERVER_PORT=$NODEMAILER_SERVER_PORT

WORKDIR /usr/src/app

# Install dependencies into temp directory
# This will cache them and speed up future builds
FROM base AS install
RUN mkdir -p /temp/dev
COPY package.json /temp/dev/
COPY bun.lockb /temp/dev/
RUN cd /temp/dev && bun install --frozen-lockfile

# Copy node_modules from temp directory
# Then copy all (non-ignored) project files into the image
FROM base AS prerelease
COPY --from=install /temp/dev/node_modules node_modules
COPY . .

# Copy production dependencies and source code into final image
FROM base AS release
COPY --from=install /temp/dev/node_modules node_modules
COPY --from=prerelease /usr/src/app .

# Create the logs directory and set permissions as root user
RUN mkdir -p /usr/src/app/logs && touch /usr/src/app/logs/infos.log /usr/src/app/logs/errors.log && chown -R bun:bun /usr/src/app/logs

# Switch to the bun user
USER bun

# Expose the application port
EXPOSE 8082

# Run the app
ENTRYPOINT [ "bun", "run", "server.js" ]