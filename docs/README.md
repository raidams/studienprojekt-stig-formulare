### Software Bill Of Materials

Open Source Frameworks:

- Backend, Spring Boot
- Frontend, Vue.js
- Entwicklungsumgebung, Docker

### Frontend Application Design

This frontend application was designed using [Figma](https://www.figma.com), a collaborative interface design tool. The application features an intuitive user interface (UI) that prioritizes usability and accessibility. The design process involved creating wireframes, interactive prototypes, and detailed visual designs to ensure a seamless user experience across different devices.

### Project links

- [ ] [Frontend Design](https://www.figma.com/file/mqY283XB7pGERzmeDHPG3Y/StiG-Portal?type=design&mode=design)
- [ ] [Powerpoint Presentation](https://1drv.ms/p/c/75843cfcffd9ca96/EQZ07s2A76BFhZJX2_kXYyIBzWxRWcYdrJgSDDCAdfujfA?e=uhyuVC)
