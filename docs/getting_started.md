### Precondition

Please install [Docker](https://www.docker.com/) and make sure docker engine is running on your system.

### Get started

To run up all services enter the following command:

```
docker compose up -d --quiet-pull
```

### Start frontend

Frontend endpoint: [Frontend URL](http://localhost:9000)

Command:

```
docker compose up -d stig_frontend --quiet-pull
```

### Start backend

Backend endpoint: [Backend URL](http://localhost:8080/api-stig-applications)

Command:

```
docker compose up -d stig_backend --quiet-pull
```

### Stop all services

Command:

```
docker compose down -v
```

### Project links

- [ ] [Api Documention](https://app.swaggerhub.com/apis-docs/robertfeo01/StiG-Portal-API/1.0.0)
