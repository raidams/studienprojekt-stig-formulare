## General information

Please use the following request mapping for all APIs.

```
http://localhost:8080/api-stig-applications/
```

## 1 Application forms

### 1.1 Create student entity \*\*\*

```
POST /applications

Body: {
    surname: <String>,
    name: <String>,
    studentNumber: <Integer>,
    email: <String>,
    degreeProgram: <String>,
    spo: <Integer>,
    application: {
        stigSemester: "eg. 3",
        semesterA: "Wintersemester or Sommersemester",
        semesterB: "Wintersemester or Sommersemester",
        approvedByStudent: <boolean>,
        approvedByDekan: <boolean>
    },
    examPlan: {
        semesterModulesA: <String[]>,
        semesterModulesB: <String[]>
    }
}
```

### 1.2 Update applications \*\*\*

```
PUT /applications/{id}

Body: {
    stig_semester: "2 or 3",
    semesterA: "Wintersemester or Sommersemester",
    semesterB: "Wintersemester or Sommersemester"
}
```

### 1.3 Approve applications \*\*\*

```
POST /applications/{id}/approvals

Body: {
    approvedByStudent: "true or false",
    approvedByDekan: "true or false",
    (approvedByCourseCoordinator "true or false")
}
```

### 1.4 List all applications \*\*\*

```
GET /applications
```

### 1.5 List applications by filter \*\*\*

```
GET /applications?studiengang={mein Studiengang, z.B. SWB}
```

### 1.6 List applications by ID \*\*\*

```
GET /applications/{id}
```

## 2 Examination plan

### 2.1 Create examination templates \*\*\*

```
POST /examination-plan-templates

Body: {
    semester,
    degreeProgram: "eg. Technische Informatik (TIB)",
    modules: [],
    spo,
}
```

### 2.2 Modify examination templates \*\*\*

```
PUT /examination-plan-templates/{id}

Body: {
    semester,
    degreeProgram: "eg. Technische Informatik (TIB)",
    modules: [],
    spo
}
```

### 2.3 List examination templates by filter \*\*\*

```
GET /examination-plan-templates?studiengang={z.B. SWB}&spo={z.B. SPO7}&semester={z.B. 3}
```

### 2.4 Create examination plans \*\*\*

```
POST /applications/{id}/examination-plans

// prüfungen inkl. bereits bestanden,

Body: {

    semesterModulesA: [
        moduleName: "e.g Mathe 1A",
        (bestanden: "true/ false",)
    ],

    semesterModulesB: [
        moduleName: "e.g Mathe 1A",
        (bestanden: "true/ false",)
    ]
}
```

### 2.5 List examination plans \*\*\*

```
GET /applications/{id}/examination-plans/{id}
```

### 2.6 Modify examination plans \*\*\*

```
PUT /applications/{id}/examination-plans/{id}

Body: {
    semesterModulesA [],
    semesterModulesB [],
}
```

## 3 Consulation hour

```
POST /consulations

Body: {
    student_number,
    surname,
    course,
    date: "dd.MM.yyyyThh:MM",
}
```

## 4 Course coordinators

### 4.1 Create entity for course coordinators \*\*\*

```
POST /degree-program-coordinators

Body: {
    title: <String>,
    surname: <String>,
    name: <String>,
    email: <String>,
    degreeProgram: <String>,
    faculty: <String>,
    administrativeSign: 'T2', 'T3' or 'TB'
}
```

### 4.2 Modify entity of course coordinators \*\*\*

```
PUT /degree-program-coordinators/{id}

Body: {
    name,
    surname,
    email,
    degreeProgram
}
```

### 4.3 Delete entity of course coordinator

```
DeLETE /degree-program-coordinators/{id}

Body: {
    surname,
    name
}
```

### 4.4 List course coordinators \*\*\*

```
GET /degree-program-coordinators
```

### 4.5 List course coordinators by ID \*\*\*

```
GET /degree-program-coordinators/{id}
```

### 4.6 List course coordinators by filter \*\*\*

```
GET /degree-program-coordinators?studiengang={z.B. SWB}
```
