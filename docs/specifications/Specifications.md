## Studienprojekt - StiG Formulare

### Anforderungen

#### Funktionale Anforderungen/ Produkt-Inkrement

#### 1 Ein Student kann:

- [ ] 1.1 Einen neuen Stig-Antrag anlegen (Attribute laut Formular + Studiengang und ggf. SPO)

- [ ] 1.2 Einen StiG Antrag bearbeiten

- [ ] 1.3 Einem StiG-Antrag einen Prüfungsplan hinzufügen

- [ ] 1.4 Einen Prüfungsplan bearbeiten

- [ ] 1.5 Einen Termin bei seinem Studiengangkoordinator anfordern

- [ ] 1.6 Einen StiG-Antrag einreichen / finalisieren

#### 2 Ein Studiengangkoordinator kann:

- [ ] 2.1 Alle StiG-Anträge seines Studiengangs einsehen (inkl. Suche und Filter)

- [ ] 2.2 Einen StiG-Antrag anlegen

- [ ] 2.3 Einen StiG-Antrag bearbeiten

- [ ] 2.4 Einem StiG-Antrag einen Prüfungsplan hinzufügen

- [ ] 2.5 Einen Prüfungsplan bearbeiten

- [ ] 2.6 (Einen StiG-Antrag genehmigen)

- [ ] 2.7 Einen StiG-Antrag als pdf drucken (im Frontend)

- [ ] 2.8 Einen Prüfungsplan als pdf drucken (im Frontend)

- [ ] 2.9 Einen StiG-Antrag inklusive Prüfungsplan als pdf drucken (im Frontend)

- [ ] 2.10 Eine Liste von StiG-Anträgen als pdf drucken (im Frontend)

#### 3 Ein Studiendekan kann:

- [ ] 3.1 Alle StiG-Anträge der Fakultät einsehen (inkl. Suche und Filter)

- [ ] 3.2 Einen StiG-Antrag anlegen

- [ ] 3.3 Einen StiG-Antrag bearbeiten

- [ ] 3.4 Einem StiG-Antrag einen Prüfungsplan hinzufügen

- [ ] 3.5 Einen Prüfungsplan bearbeiten

- [ ] 3.6 Einen StiG-Antrag genehmigen

- [ ] 3.7 Einen StiG-Antrag als pdf drucken (im Frontend)

- [ ] 3.8 Einen Prüfungsplan als pdf drucken (im Frontend)

- [ ] 3.9 Einen StiG-Antrag inklusive Prüfungsplan als pdf drucken (im Frontend)

- [ ] 3.10 Eine Liste von StiG-Anträgen als pdf drucken (im Frontend)
