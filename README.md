### General Information

This project was developed for Hochschule Esslingen University of Applied Science to efficiently manage study-specific forms. These forms are designed to capture individual study speeds and are sent to both the dean and the degree coordinator for review. By streamlining this process, the project ensures accurate and timely communication between students and faculty.

### Software components

The software consists of the following services:

- [ ] frontend service
- [ ] authentification service
- [ ] e-mail service
- [ ] backend service
- [ ] database service
